## Name
STM32F0 Air Quality Monitor HAT

## Description
Air Quality Monitor PCB is an [STM32F051R8T6](https://www.mouser.co.za/datasheet/2/389/dm00039193-1797631.pdf) HAT used to detect gas leakages such as methane in indoor areas by using [MQ4 gas sensor](https://www.sparkfun.com/datasheets/Sensors/Biometric/MQ-4.pdf). The HAT is also integrated with an STH30-DIS temp/hum sensor to monitor the air conditions of the environment where that HAT will be operated. The HAT is separated into three subsystems: Power, Sensing and Microcontroller subsystems. The power subsystem provides 3.3V to the other subsystems. The board is powered by a 3.7V Li-on battery. The HAT has a green LED to indicate good air conditions and a red LED to indicate the presence of gas leakages.

## Software
Install [STMCubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html) to configure the HAT pins and program the microcontroller.

## Operation 

The HAT takes in 3.7V from the battery and uses a step-up converter which steps up 3.7V to 5V which is used to charge the battery. The 5V line is supplied to the MQ4 gas sensor which starts to operate and measure the gas concentrations and the expected output is an analog signal which is a linear response and starts to inccrease exponentially when the natural gases/methane is detected and decays when the air conditions are good. The HAT regulates 5V to 3.3V which is supplied to the temp/hum which starts to measure digital data when there's a 400kHz-1MHz clock  signal. 

Expected output of the MQ4 gas sensor can be found [here](https://cdn.sparkfun.com/datasheets/Sensors/Biometric/MQ-4%20Ver1.3%20-%20Manual.pdf)
Expected outpout of the temp/hum sensor can be found [here](https://media.digikey.com/pdf/Data%20Sheets/Sensirion%20PDFs/HT_DS_SHT3x_DIS.pdf) 

## HAT-STM32F0 Connections
The HAT uses I2C communication protocol.

SENSORS
Digital pin PB7 is connected to the serial data pin (SDA) of the temp/hum sensor to transfer data to and from the sensor and pin PB6 is connected to the serial clock (SCL) of the temp/hum sensor used to synchronice communication between the microcontroller and the sensor at a frequency of less than 1MHz. Analog pin PC5 is connected to the output of the MQ4 gas sensor.

FTDI
STM32 PA2 is connected to TXD, PA3 is connected to RXD, RTS is connected to PA1 and CTS is connected to PA3

MEMORY
Digital pin PB9 is connected to the serial data pin (SDA) of the eeprom and pin PB8 is connected to the serial clock (SCL) 

OUTPUTS
The green LED is connected to PB5 and the red LED is connected to PB5 

## Support
If you have issues, suggestions, ideas or need assistance, please do not hesitate to make an [issue](https://gitlab.com/eee3088f-group-30/stm-32-hat/-/issues) or contact the contributors [here](https://gitlab.com/eee3088f-group-30/stm-32-hat/-/project_members).

## Roadmap
### Mechanical Design
The HAT will have a protective case 

### Display Interface (LCD)
The HAT will be integrated with an LCD to display gas concentration levels, temperature and humidity from the sensors. The LCD can also be used to monitor the charging percentage of the battery.

### Alarm System Interface 
This feature will be used to send a signal to an alarm system panel to notify the user when the gas levels are above the desired threshold

### Bluetooth Interface
This module will be used for short-range 2.4G wireless communication with the HAT.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## Authors and acknowledgment
Authors: Kingsley Macebele, Oscar Chauke and Hendry Klaas

Special thanks to Dr. Jane Wyngaard, Justin Pead, Samuel (University of Cape Town) for their contributions in this project. 

## License
[MIT](https://choosealicense.com/licenses/mit/)
